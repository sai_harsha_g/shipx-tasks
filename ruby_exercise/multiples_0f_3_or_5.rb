def multiples_of_3_or_5(num)
  sum = 0
  num = num.to_i
  for i in 3...num
    if (i%3==0 || i%5==0)
      sum += i
    end
  end
  puts "the sum of all the multiples of 3 or 5 below #{num} is #{sum}."
end

puts "Enter the number : "
n = gets.chomp()  
multiples_of_3_or_5(n)
