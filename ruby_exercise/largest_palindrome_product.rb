def checking_palindrome(num)
  num = num.to_i
  temp=num
  sum = 0
  while num!=0  #implementation of while loop
    rem=num%10
    num=num/10
    sum=sum*10+rem
  end

  if(temp==sum)
    return true
  else
    return false
  end
end

def largest_palindrome_product
  result = []
  for i in 999.downto(100)
    for j in 999.downto(100)
      k = i*j
      if checking_palindrome(k)
        result.push(k)
      end
    end
  end
  result = result.sort
  l = result.length
  puts "the largest palindrome made from the product of two 3-digit numbers is #{result[l-1]}"
end

largest_palindrome_product
