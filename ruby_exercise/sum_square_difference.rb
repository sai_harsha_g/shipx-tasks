def sum_square_difference
  numbers  = (1..100).to_a
  sum_of_squares = 0
  sum_of_numbers  = 0
  numbers.each do |num|
    sum_of_numbers +=  num
    sum_of_squares += (num ** 2)
  end
  puts ((sum_of_numbers ** 2) - sum_of_squares)
end

sum_square_difference
