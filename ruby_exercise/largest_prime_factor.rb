require 'prime'

def largest_prime_factor(num)
  factor, largest_value = 0
  num = num.to_i
  n = num/2.floor + 1
  for i in n.downto(2) 
    if num%i==0
      factor = i
      if factor.prime?
      	largest_value = factor
        break
      end
    end
  end
  puts "largest prime factor of #{num} is #{largest_value}"
end

puts "Enter a number : "
k = gets.chomp()
largest_prime_factor(k)
