1) Disk usage statistics for root folder (available diskspace, used and free in GBs)

	>> df -h 
	[df --display filesystem | -h --human readable]
	
2) Memory (RAM) usage statistics (total memory available, used, and free in MBs) 

	>> free --mega
	[total amount of free space available along with the amount of memory used | --mega Display the amount of memory in megabytes]
	
3)  What process is using the max memory? 

	>> htop
	
4) Which folder or file in /usr/bin is taking most amount of diskspace?

	>> ps aux --sort=-%mem | head -n 2
	[ps --process status, -aux for Select all processes | sort -n for number sorting | head -n 2 for first 2 lines]

5) Find the chrome processes currently running?

	>> ps -ef | grep chrome
	[ps --process status | -ef for Select all processes | grep will select all the lines with chrome process]
	
6) Find the total number of chrome process running?

	>> ps -ef | grep chrome | wc -l
	[ ps --process status | -ef for Select all processes | wc -l gives total number of lines]

7) Find the RSS (Resident non-swapped memory usage) for each process?

	>> ps -eo rss
	[ps --process status | -e for Select all processes, -o for format | -F for full format listing including RSS]

8) Get the pids of all chrome process currently running?

	>> ps -A | grep chrome | cut -d " " -f 4
	[ps --process status | -A for Select all processes | grep will select all the lines with chrome process | cut gives the process id for each chrome process]

9) Write a script that will log the memory utilization to a file along with current data and time. Every time script is run, the information should be added to the same file.  Figure out how this script can be scheduled so that it will automatically run every 10 min and we can have memory util info for every 10 min ?

       >> in check.sh
		free -h >> ~/log.txt
		date >> ~/log.txt
	
	>>in terminal type crontab -e  [to open crontab]
		*/10 * * * * bash ~/check.sh
