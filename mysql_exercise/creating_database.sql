create database shipx_task;

use shipx_task;

create table employees
	( employee_id int primary key not null auto_increment,
	  name varchar(255),
	  city varchar(255),
	  state varchar(255),
	  age int,
      department_id int);

create table departments     
	( department_id int primary key not null Auto_increment,
      department_name varchar(255),
	  group_name varchar(255));
      
insert into employees(name,city,state,age,department_id)
	 values
		('Bindu','Chennai','Tamil Nadu',37,2),
		('Chethan','Hyderabad','Telangana',39,1),
		('Danny','Bengaluru','Karnataka', 26, 1),
		('Eva','Chennai','Tamil Nadu',27,2),
		('Fatima','Hyderabad','Telangana',25,3),
		('Gaurav','Mumbai','Maharashtra',29,2),
		('HImesh','New Delhi','Delhi',31,1),
		('Indu','Bengaluru','Karnataka',26,2),
		('Jayan','Chennai','Tamil Nadu',28,1),
		('Ketan','Mumbai','Maharashtra',29,3),
		('Lisa','Mysuru','Karnataka',31,4),
		('Manu','Mysuru','Karnataka',27,1),
		('Nandin','Chennai','Tamil Nadu',30,3),
		('Osman','New Delhi','Delhi',32,4),
		('Peter','Bengaluru','Karnataka',35,1);
                
insert into departments(department_name,group_name)
	values
        ('Development','Engineering'),
		('QA', 'Engineering'),
		('Consulting','Services'),
		('Support','Services'),
		('Sales','Sales');