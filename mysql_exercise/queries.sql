use shipx_task;
select employee_id,name,city,state from shipx_task.employees;

select name from shipx_task.employees
where age>30;

select name from shipx_task.employees
where city = 'Bengaluru' and state = 'Karnataka';

select city, state, avg(age) as 'average age' from shipx_task.employees
group by city, state;

select city, count(employee_id) as 'employees count' from shipx_task.employees
where state = 'Karnataka'
group by city;

select city, count(employee_id) as 'employees count' from shipx_task.employees
group by city
having count(employee_id) >= 3;

select e.name as 'employee_name', d.department_name, d.group_name from shipx_task.employees e
join shipx_task.departments d
where e.department_id = d.department_id;

select e.name as 'employee_name', e.age from shipx_task.employees e
join shipx_task.departments d
where e.department_id = d.department_id and d.group_name = 'Engineering';

select department_name from shipx_task.departments
where department_id not in (SELECT distinct(department_id) from shipx_task.employees);

select d.department_name, MAX(age) as 'age' from shipx_task.employees e
join shipx_task.departments d 
where e.department_id=d.department_id 
group by department_name
UNION
select department_name, 0 as 'age' from  shipx_task.departments 
where department_id not in (select distinct(department_id) from shipx_task.employees);

select d.department_name, MAX(age) as 'age' from shipx_task.employees e
right join shipx_task.departments d 
on e.department_id=d.department_id
group by d.department_name;


alter table employees add pincode varchar(255);

alter table employees modify name varchar(512);

use shipx_task;
CREATE INDEX IX_new_table
	ON employees(city, state);